package model.data_structures;

import java.util.Arrays;

public class MinBinHeap <T extends Comparable<T>> implements IMinBinHeap<T>{

	private static final int DEFAULT_CAPACITY = 10;
	protected T[] keys;
	protected int size;

	public MinBinHeap () {
		keys = (T[])new Comparable[DEFAULT_CAPACITY];  
		size = 0;
	}


	public void insert(T value) {

		if (size >= keys.length - 1) {
			keys = this.resize();
		}        
		size++;
		int index = size;
		keys[index] = value;

		swim();
	}



	public T peek() {
		if (this.isEmpty()) {
			throw new IllegalStateException();
		}

		return keys[1];
	}


	public T removeMinimum() {
		
		T result = peek();

		keys[1] = keys[size];
		keys[size] = null;
		size--;

		sink();

		return result;
	}


	public void sink() {
		int index = 1;
		while (hasLeftChild(index)) {
			int smallerChild = leftIndex(index);

			if (hasRightChild(index)
					&& keys[leftIndex(index)].compareTo(keys[rightIndex(index)]) > 0) {
				smallerChild = rightIndex(index);
			} 

			if (keys[index].compareTo(keys[smallerChild]) > 0) {
				exch(index, smallerChild);
			} else {
				break;
			}
			index = smallerChild;
		}        
	}


	public void swim() {
		int index = this.size;

		while (hasParent(index)
				&& (parent(index).compareTo(keys[index]) > 0)) {
			exch(index, parentIndex(index));
			index = parentIndex(index);
		}        
	}
	public T[] darKeys() {
		return keys;
	}

	public boolean isEmpty() {
		return size == 0;
	}


	public  boolean hasParent(int i) {
		return i > 1;
	}


	public int leftIndex(int i) {
		return i * 2;
	}


	public int rightIndex(int i) {
		return i * 2 + 1;
	}


	public boolean hasLeftChild(int i) {
		return leftIndex(i) <= size;
	}


	public boolean hasRightChild(int i) {
		return rightIndex(i) <= size;
	}


	public T parent(int i) {
		return keys[parentIndex(i)];
	}


	public int parentIndex(int i) {
		return i / 2;
	}


	public T[] resize() {
		return Arrays.copyOf(keys, keys.length * 2);
	}


	public void exch(int index1, int index2) {
		T tmp = keys[index1];
		keys[index1] = keys[index2];
		keys[index2] = tmp;        
	}
}


