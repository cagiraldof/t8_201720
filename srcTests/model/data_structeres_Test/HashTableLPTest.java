package model.data_structeres_Test;


import junit.framework.TestCase;
import model.data_structures.HashTableLP;


public class HashTableLPTest extends TestCase{

	private HashTableLP<String, Object> ht= new HashTableLP<>();


	public void setupEscenario1(){
		String k1="1";
		int v1=1;

		ht.put(k1, v1);
	}

	public void setupEscenario2(){

		String k2="2";
		int i2 = 2;
		String k3="3";
		int i3 = 3;
		String k4="4";
		int i4 = 4;

		ht.put(k2, i2);
		ht.put(k3, i3);
		ht.put(k4, i4);
	}



	public void testIsEmpty(){

		assertTrue("Se esperaba true", ht.isEmpty());

		setupEscenario1();
		assertFalse("Se esperaba false" , ht.isEmpty());
	}

	public void testPut(){

		setupEscenario1();
		assertEquals("Se esperaba 1", 1, ht.get("1"));


		setupEscenario2();
		assertEquals("Se esperaba 2", 2, ht.get("2"));
		assertEquals("Se esperaba 3", 3, ht.get("3"));
		assertEquals("Se esperaba 4", 4, ht.get("4"));
	}

	public void testDelete(){

		setupEscenario1();
		assertFalse("Se esperaba false" , ht.isEmpty());
		ht.delete("1");
		assertEquals("Se esperaba null", null, ht.get("1"));

		setupEscenario2();
		assertEquals("Se esperaba 2", 2, ht.get("2"));
		ht.delete("2");
		assertEquals("Se esperaba null", null, ht.get("2"));
		assertEquals("Se esperaba 3", 3, ht.get("3"));
		ht.delete("3");
		assertEquals("Se esperaba null", null, ht.get("3"));
		assertEquals("Se esperaba 4", 4, ht.get("4"));
		ht.delete("4");
		assertEquals("Se esperaba null", null, ht.get("4"));


		assertTrue("Se esperaba true", ht.isEmpty());
	}
	
		public void testSize(){
	
			assertEquals("Se esperaba tama�o 0",0, ht.darN());
			setupEscenario1();
			assertEquals("Se esperaba tama�o 1",1, ht.darN());
	

			setupEscenario2();
			assertEquals("Se esperaba tama�o 4",4, ht.darN());
			
			String k5="5";
			int i5 = 5;
			String k6="6";
			int i6 = 6;
			String k7="7";
			int i7 = 7;

			ht.put(k5, i5);
			ht.put(k6, i6);
			ht.put(k7, i7);
			
			assertEquals("Se esperaba tama�o 7",7, ht.darN());
			
			ht.delete("1");
			assertEquals("Se esperaba tama�o 6",6, ht.darN());
			
			ht.delete("3");
			ht.delete("5");
			ht.delete("6");
			assertEquals("Se esperaba tama�o 3",3, ht.darN());
	      
		}
}
