package model.vo;

import model.data_structures.IList;

public class VOParada implements Comparable<VOParada>

{
	//Atributos
	
	/**
	 * Modela el id de la parada
	 */
	private int stop_id;
	private int stop_code;
	private String stop_name;
	private String stop_desc;
	private double stop_lat;
	private double stop_lon;
	private String zone_id;
	private String stop_url;
	private int location_type;
	private String parent_station;
	/**
	 * Modela el n�mero de incidentes
	 */
	private int numeroIncidentes;
	
	public VOParada(String pStop_Id,String pStop_Code, String pStop_Name, String pStop_Desc, String pStop_Lat,
			String pStop_Lon, String pZone_Id, String pStop_Url, String pLocation_Type, String pParent_Station){
		try {
			stop_id = Integer.parseInt(pStop_Id);
			stop_code = Integer.parseInt(pStop_Code);
			stop_name = pStop_Name;
			stop_desc = pStop_Desc;
			stop_lat = Double.parseDouble(pStop_Lat);
			stop_lon = Double.parseDouble(pStop_Lon);
			zone_id = pZone_Id;
			stop_url = pStop_Url;
			location_type = Integer.parseInt(pLocation_Type);
			parent_station = pParent_Station;	
			numeroIncidentes=0;
		}
		catch (Exception e) {
			//Hay un error en el txt con Stop_Code
			pStop_Code="0";

			stop_code = Integer.parseInt(pStop_Code);

		}
	}
	
	/**
	 * Si la parada es compartida, esta lista tiene las rutas que la comparten
	 * , cada una con los viajes que usan la parada. 
	 */
	private IList<VOViajesRuta4C> viajesDeRutas;
	
	public IList<VOViajesRuta4C> getViajesDeRutas() {
		return viajesDeRutas;
	}

	public void setViajesDeRutas(IList<VOViajesRuta4C> viajesDeRutas) {
		this.viajesDeRutas = viajesDeRutas;
	}
	
	//M�todos

	/**
	 * @return the stop_id
	 */
	public int getStop_id() {
		return stop_id;
	}

	/**
	 * @param stop_id the stop_id to set
	 */
	public void setStop_id(int stop_id) {
		this.stop_id = stop_id;
	}

	/**
	 * @return the stop_code
	 */
	public int getStop_code() {
		return stop_code;
	}

	/**
	 * @param stop_code the stop_code to set
	 */
	public void setStop_code(int stop_code) {
		this.stop_code = stop_code;
	}

	/**
	 * @return the stop_name
	 */
	public String getStop_name() {
		return stop_name;
	}

	/**
	 * @param stop_name the stop_name to set
	 */
	public void setStop_name(String stop_name) {
		this.stop_name = stop_name;
	}

	/**
	 * @return the stop_desc
	 */
	public String getStop_desc() {
		return stop_desc;
	}

	/**
	 * @param stop_desc the stop_desc to set
	 */
	public void setStop_desc(String stop_desc) {
		this.stop_desc = stop_desc;
	}

	/**
	 * @return the stop_lat
	 */
	public double getStop_lat() {
		return stop_lat;
	}

	/**
	 * @param stop_lat the stop_lat to set
	 */
	public void setStop_lat(double stop_lat) {
		this.stop_lat = stop_lat;
	}

	/**
	 * @return the stop_lon
	 */
	public double getStop_lon() {
		return stop_lon;
	}

	/**
	 * @param stop_lon the stop_lon to set
	 */
	public void setStop_lon(double stop_lon) {
		this.stop_lon = stop_lon;
	}

	/**
	 * @return the zone_id
	 */
	public String getZone_id() {
		return zone_id;
	}

	/**
	 * @param zone_id the zone_id to set
	 */
	public void setZone_id(String zone_id) {
		this.zone_id = zone_id;
	}

	/**
	 * @return the stop_url
	 */
	public String getStop_url() {
		return stop_url;
	}

	/**
	 * @param stop_url the stop_url to set
	 */
	public void setStop_url(String stop_url) {
		this.stop_url = stop_url;
	}

	/**
	 * @return the location_type
	 */
	public int getLocation_type() {
		return location_type;
	}

	/**
	 * @param location_type the location_type to set
	 */
	public void setLocation_type(int location_type) {
		this.location_type = location_type;
	}

	/**
	 * @return the parent_station
	 */
	public String getParent_station() {
		return parent_station;
	}

	/**
	 * @param parent_station the parent_station to set
	 */
	public void setParent_station(String parent_station) {
		this.parent_station = parent_station;
	}
	/**
	 * @return the numeroIncidentes
	 */
	public int getNumeroIncidentes() {
		return numeroIncidentes;
	}

	/**
	 * @param numeroIncidentes the numeroIncidentes to set
	 */
	public void setNumeroIncidentes(int numeroIncidentes) {
		this.numeroIncidentes = numeroIncidentes;
	}

	@Override
	public int compareTo(VOParada o) {
		return this.numeroIncidentes-o.getNumeroIncidentes();
	}


	
}
