package model.vo;

public class VOAgency {
	
	private String agencyID;
	private String name;
	private String URL;
	private String timezone;
	private String language;
	/**
	 * @param agencyID
	 * @param name
	 * @param uRL
	 * @param timezone
	 * @param language
	 */
	public VOAgency(String agencyID, String name, String uRL, String timezone, String language) {
		super();
		this.agencyID = agencyID;
		this.name = name;
		URL = uRL;
		this.timezone = timezone;
		this.language = language;
	}
	/**
	 * @return the agencyID
	 */
	public String getAgencyID() {
		return agencyID;
	}
	/**
	 * @param agencyID the agencyID to set
	 */
	public void setAgencyID(String agencyID) {
		this.agencyID = agencyID;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the uRL
	 */
	public String getURL() {
		return URL;
	}
	/**
	 * @param uRL the uRL to set
	 */
	public void setURL(String uRL) {
		URL = uRL;
	}
	/**
	 * @return the timezone
	 */
	public String getTimezone() {
		return timezone;
	}
	/**
	 * @param timezone the timezone to set
	 */
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}
	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	
}
