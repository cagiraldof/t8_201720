package model.data_structures;

import java.util.Iterator;

public interface IQueue<E> extends Iterable<E>{
	
	public void enqueue(E item);
	
	public E dequeue();
	public int size();
}
