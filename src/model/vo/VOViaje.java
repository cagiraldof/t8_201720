package model.vo;

public class VOViaje 
{

	private int routeID;
	private int serviceID;
	private int tripID;
	private String tripHeadsign;
	private String tripShortName;
	private int directionID;
	private int blockID;
	private int shapeID;
	private boolean wheelchairAcc;
	private boolean bikesAll;
	
	/**
	 * @param routeID
	 * @param serviceID
	 * @param tripID
	 * @param tripHeadsign
	 * @param tripShortName
	 * @param directionID
	 * @param blockID
	 * @param shapeID
	 * @param wheelchairAcc
	 * @param bikesAll
	 */
	public VOViaje(int routeID, int serviceID, int tripID, String tripHeadsign, String tripShortName, int directionID,
			int blockID, int shapeID, boolean wheelchairAcc, boolean bikesAll) {
		super();
		this.routeID = routeID;
		this.serviceID = serviceID;
		this.tripID = tripID;
		this.tripHeadsign = tripHeadsign;
		this.tripShortName = tripShortName;
		this.directionID = directionID;
		this.blockID = blockID;
		this.shapeID = shapeID;
		this.wheelchairAcc = wheelchairAcc;
		this.bikesAll = bikesAll;
	}
	/**
	 * @return the routeID
	 */
	public int getRouteID() {
		return routeID;
	}
	/**
	 * @param routeID the routeID to set
	 */
	public void setRouteID(int routeID) {
		this.routeID = routeID;
	}
	/**
	 * @return the serviceID
	 */
	public int getServiceID() {
		return serviceID;
	}
	/**
	 * @param serviceID the serviceID to set
	 */
	public void setServiceID(int serviceID) {
		this.serviceID = serviceID;
	}
	/**
	 * @return the tripID
	 */
	public int getTripID() {
		return tripID;
	}
	/**
	 * @param tripID the tripID to set
	 */
	public void setTripID(int tripID) {
		this.tripID = tripID;
	}
	/**
	 * @return the tripHeadsign
	 */
	public String getTripHeadsign() {
		return tripHeadsign;
	}
	/**
	 * @param tripHeadsign the tripHeadsign to set
	 */
	public void setTripHeadsign(String tripHeadsign) {
		this.tripHeadsign = tripHeadsign;
	}
	/**
	 * @return the tripShortName
	 */
	public String getTripShortName() {
		return tripShortName;
	}
	/**
	 * @param tripShortName the tripShortName to set
	 */
	public void setTripShortName(String tripShortName) {
		this.tripShortName = tripShortName;
	}
	/**
	 * @return the directionID
	 */
	public int getDirectionID() {
		return directionID;
	}
	/**
	 * @param directionID the directionID to set
	 */
	public void setDirectionID(int directionID) {
		this.directionID = directionID;
	}
	/**
	 * @return the blockID
	 */
	public int getBlockID() {
		return blockID;
	}
	/**
	 * @param blockID the blockID to set
	 */
	public void setBlockID(int blockID) {
		this.blockID = blockID;
	}
	/**
	 * @return the shapeID
	 */
	public int getShapeID() {
		return shapeID;
	}
	/**
	 * @param shapeID the shapeID to set
	 */
	public void setShapeID(int shapeID) {
		this.shapeID = shapeID;
	}
	/**
	 * @return the wheelchairAcc
	 */
	public boolean isWheelchairAcc() {
		return wheelchairAcc;
	}
	/**
	 * @param wheelchairAcc the wheelchairAcc to set
	 */
	public void setWheelchairAcc(boolean wheelchairAcc) {
		this.wheelchairAcc = wheelchairAcc;
	}
	/**
	 * @return the bikesAll
	 */
	public boolean isBikesAll() {
		return bikesAll;
	}
	/**
	 * @param bikesAll the bikesAll to set
	 */
	public void setBikesAll(boolean bikesAll) {
		this.bikesAll = bikesAll;
	}


}
