package model.vo;

import java.sql.Time;

public class VOBusUpdate {
	private int vehicleNo;
	private int tripID;
	private int routeNo;
	private String direction;
	private String destination;
	private String pattern;
	private Double latitude;
	private Double longitude;
	private Time recordedTime;
	private VORouteMap routeMap;

	public VOBusUpdate(int vehicleNo, int tripID, int routeNo, String direction, String destination, String pattern,
			Double latitude, Double longitude, Time recordedTime, VORouteMap routeMap) {
		super();
		this.vehicleNo = vehicleNo;
		this.tripID = tripID;
		this.routeNo = routeNo;
		this.direction = direction;
		this.destination = destination;
		this.pattern = pattern;
		this.latitude = latitude;
		this.longitude = longitude;
		this.recordedTime = recordedTime;
		this.routeMap = routeMap;
	}

	public int getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(int vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public int getTripID() {
		return tripID;
	}

	public void setTripID(int tripID) {
		this.tripID = tripID;
	}

	public int getRouteNo() {
		return routeNo;
	}

	public void setRouteNo(int routeNo) {
		this.routeNo = routeNo;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Time getRecordedTime() {
		return recordedTime;
	}

	public void setRecordedTime(Time recordedTime) {
		this.recordedTime = recordedTime;
	}

	public VORouteMap getRouteMap() {
		return routeMap;
	}

	public void setRouteMap(VORouteMap routeMap) {
		this.routeMap = routeMap;
	}




}
