package model.vo;
import model.data_structures.*;;


/**
 * Clase enfocada en el retorno del requerimiento 4C, que referencia la ruta que est� usando
 *  la parada compartida y los viajes de dicha ruta que la usan.
 * @author Daniel
 *
 */
public class VOViajesRuta4C {
	
	/**
	 * id de la ruta que usa la parada.
	 */
	private Long idRuta;
	
	/**
	 * id de la Parada
	 */
	private Long idParadaCompartida;
	
	/**
	 * Lista de los viajes de la ruta, que usan la parada.
	 */
	private IList<VOViaje> viajes;

	public Long getIdRuta() {
		return idRuta;
	}

	public void setIdRuta(Long idRuta) {
		this.idRuta = idRuta;
	}

	public Long getIdParadaCompartida() {
		return idParadaCompartida;
	}

	public void setIdParadaCompartida(Long idParadaCompartida) {
		this.idParadaCompartida = idParadaCompartida;
	}

	public IList<VOViaje> getViajes() {
		return viajes;
	}

	public void setViajes(IList<VOViaje> viajes) {
		this.viajes = viajes;
	}
	
	
	
	
	

}
