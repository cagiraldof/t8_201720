package model.data_structures;

public interface IMinBinHeap<T> {
	public void insert(T value);
	public T peek();
	public T removeMinimum();
	public void sink();
	public T[] darKeys() ;
	public boolean isEmpty();
	public  boolean hasParent(int i);
	public int leftIndex(int i);
	
	
}
