package model.vo;

public class VORouteMap {
public String href;

public VORouteMap(String href) {
	super();
	this.href = href;
}

public String getHref() {
	return href;
}

public void setHref(String href) {
	this.href = href;
}

}
