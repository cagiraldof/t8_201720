package model.data_structures;

public class EdgeWeightedDigraph {


	private int V;                
	private int E;                      
	private Bag<DirectedEdge> [] adj;    
	private int[] numeroAdyacentes;             


	public EdgeWeightedDigraph() {

		this.V = 0;
		this.E = 0;

	}

	public int V() {
		return V;
	}


	public int E() {
		return E;
	}


	public void addEdge(SimpleNode source, SimpleNode dest, double weight ) {

		DirectedEdge e =new DirectedEdge(source, dest, weight);
		adj.add(e);
		E++;
	}



	public Iterable<DirectedEdge> adj(int v) {

		return adj[v];
	}


	public int outdegree(int v) {

		return adj[v].size();
	}


	public int numeroAdyacentes(int v) {

		return numeroAdyacentes[v];
	}


	public Iterable<DirectedEdge> edges() {
		Bag<DirectedEdge> list = new Bag<DirectedEdge>();
		for (int v = 0; v < V; v++) {
			for (DirectedEdge e : adj(v)) {
				list.add(e);
			}
		}
		return list;
	} 

	


}