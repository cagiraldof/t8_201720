package model.vo;

import java.sql.Time;

public class VOBusEstimUpdate {
	private int routeID;
	private String routeName;
	private String direction;
	private VORouteMap routeMap;
	private VOSchedule[] schedules;

	public VOBusEstimUpdate(int routeID, String routeName, String direction, VORouteMap routeMap,
			VOSchedule[] schedules) {
		super();
		this.routeID = routeID;
		this.routeName = routeName;
		this.direction = direction;
		this.routeMap = routeMap;
		this.schedules = schedules;
	}

	/**
	 * @return the routeID
	 */
	public int getRouteID() {
		return routeID;
	}

	/**
	 * @param routeID the routeID to set
	 */
	public void setRouteID(int routeID) {
		this.routeID = routeID;
	}

	/**
	 * @return the routeName
	 */
	public String getRouteName() {
		return routeName;
	}

	/**
	 * @param routeName the routeName to set
	 */
	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}

	/**
	 * @return the direction
	 */
	public String getDirection() {
		return direction;
	}

	/**
	 * @param direction the direction to set
	 */
	public void setDirection(String direction) {
		this.direction = direction;
	}

	/**
	 * @return the routeMap
	 */
	public VORouteMap getRouteMap() {
		return routeMap;
	}

	/**
	 * @param routeMap the routeMap to set
	 */
	public void setRouteMap(VORouteMap routeMap) {
		this.routeMap = routeMap;
	}

	/**
	 * @return the schedules
	 */
	public VOSchedule[] getSchedules() {
		return schedules;
	}

	/**
	 * @param schedules the schedules to set
	 */
	public void setSchedules(VOSchedule[] schedules) {
		this.schedules = schedules;
	}

	public class VOSchedule {
		private String pattern;
		private String destination;
		private Time expectedLeaveTime;
		private int expectedCountDown;
		private char scheduleStatus;
		private boolean cancelledTrip;
		private boolean cancelledStop;
		private boolean addedTrip;
		private boolean addedStop;
		private Time lastUpdate;

		public VOSchedule(String pattern, String destination, Time expectedLeaveTime, int expectedCountDown,
				char scheduleStatus, boolean cancelledTrip, boolean cancelledStop, boolean addedTrip, boolean addedStop,
				Time lastUpdate) {
			super();
			this.pattern = pattern;
			this.destination = destination;
			this.expectedLeaveTime = expectedLeaveTime;
			this.expectedCountDown = expectedCountDown;
			this.scheduleStatus = scheduleStatus;
			this.cancelledTrip = cancelledTrip;
			this.cancelledStop = cancelledStop;
			this.addedTrip = addedTrip;
			this.addedStop = addedStop;
			this.lastUpdate = lastUpdate;
		}

		/**
		 * @return the pattern
		 */
		public String getPattern() {
			return pattern;
		}
		/**
		 * @param pattern the pattern to set
		 */
		public void setPattern(String pattern) {
			this.pattern = pattern;
		}
		/**
		 * @return the destination
		 */
		public String getDestination() {
			return destination;
		}
		/**
		 * @param destination the destination to set
		 */
		public void setDestination(String destination) {
			this.destination = destination;
		}
		/**
		 * @return the expectedLeaveTime
		 */
		public Time getExpectedLeaveTime() {
			return expectedLeaveTime;
		}
		/**
		 * @param expectedLeaveTime the expectedLeaveTime to set
		 */
		public void setExpectedLeaveTime(Time expectedLeaveTime) {
			this.expectedLeaveTime = expectedLeaveTime;
		}
		/**
		 * @return the expectedCountDown
		 */
		public int getExpectedCountDown() {
			return expectedCountDown;
		}
		/**
		 * @param expectedCountDown the expectedCountDown to set
		 */
		public void setExpectedCountDown(int expectedCountDown) {
			this.expectedCountDown = expectedCountDown;
		}
		/**
		 * @return the scheduleStatus
		 */
		public char getScheduleStatus() {
			return scheduleStatus;
		}
		/**
		 * @param scheduleStatus the scheduleStatus to set
		 */
		public void setScheduleStatus(char scheduleStatus) {
			this.scheduleStatus = scheduleStatus;
		}
		/**
		 * @return the cancelledTrip
		 */
		public boolean isCancelledTrip() {
			return cancelledTrip;
		}
		/**
		 * @param cancelledTrip the cancelledTrip to set
		 */
		public void setCancelledTrip(boolean cancelledTrip) {
			this.cancelledTrip = cancelledTrip;
		}
		/**
		 * @return the cancelledStop
		 */
		public boolean isCancelledStop() {
			return cancelledStop;
		}
		/**
		 * @param cancelledStop the cancelledStop to set
		 */
		public void setCancelledStop(boolean cancelledStop) {
			this.cancelledStop = cancelledStop;
		}
		/**
		 * @return the addedTrip
		 */
		public boolean isAddedTrip() {
			return addedTrip;
		}
		/**
		 * @param addedTrip the addedTrip to set
		 */
		public void setAddedTrip(boolean addedTrip) {
			this.addedTrip = addedTrip;
		}
		/**
		 * @return the addedStop
		 */
		public boolean isAddedStop() {
			return addedStop;
		}
		/**
		 * @param addedStop the addedStop to set
		 */
		public void setAddedStop(boolean addedStop) {
			this.addedStop = addedStop;
		}
		/**
		 * @return the lastUpdate
		 */
		public Time getLastUpdate() {
			return lastUpdate;
		}
		/**
		 * @param lastUpdate the lastUpdate to set
		 */
		public void setLastUpdate(Time lastUpdate) {
			this.lastUpdate = lastUpdate;
		}


	}

}
