package controller;

import API.ISTSManager;
import model.data_structures.IHashTableLP;
import model.data_structures.IList;
import model.data_structures.IRedBlackBST;
import model.exceptions.DateNotFoundExpection;
import model.logic.STSManager;
import model.vo.*;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static ISTSManager manager  = new STSManager();
	private static String fecha;

	public static void ITSInit() 
	{
		manager.ITSInit();
	}
	

	 
	public static void ITScargarGTFS1C() 
	{
		manager.ITScargarGTFS1C();	
	}

	 
	public static void ITScargarTR(String fechaCarga) throws DateNotFoundExpection
	{
		fecha = fechaCarga;
		manager.ITScargarTR(fecha);
	}

	

	 
	public static IList<VOViaje> ITSviajesHuboRetardoParadas1A(String idRuta) {
		 
		return manager.ITSviajesHuboRetardoParadas1A(idRuta, fecha);
	}

	 
	public static IList<VOParada> ITSNParadasMasRetardos2A(int n) {
		 
		return manager.ITSNParadasMasRetardos2A(fecha, n);
	}

	 
	public static IList<VOTransbordo> ITStransbordosRuta3A(String idRuta) {
		 
		return manager.ITStransbordosRuta3A(idRuta, fecha);
	}

	 
	public static IList<VOViaje> ITSviajesRetrasoTotalRuta1B(String idRuta)
	{
		 
		return manager.ITSviajesRetrasoTotalRuta1B(idRuta, fecha);
	}

	 
	public static VORangoHora ITSretardoHoraRuta2B(String idRuta)
	{
		 
		return manager.ITSretardoHoraRuta2B(idRuta, fecha);
	}

	 
	public static IList<VOViaje> ITSbuscarViajesParadas3B(String idOrigen, String idDestino, String horaInicio,
			String horaFin) 
	{
		 
		return manager.ITSbuscarViajesParadas3B(idOrigen, idDestino, fecha, horaInicio, horaFin);
	}

	 
	public static IList<VOViaje> ITSNViajesMasDistancia2C(int n) {
		 
		return manager.ITSNViajesMasDistancia2C(n, fecha);
	}

	 
	public static IRedBlackBST<Integer, IList<VORetardo>> ITSRetardosViajeFecha3C(String idViaje) {
		 
		return manager.retardosViajeFecha3C(idViaje, fecha);
	}

	 
	public static IList<VOParada> ITSParadasCompartidas4C()
	{
		 
		return manager.ITSParadasCompartidas4C(fecha);
	}
	
	public static IList<VOParadasViaje> ITSViajesPararonEnRango5C(String idRuta, String horaInicio, String horaFin){
		return manager.ITSViajesPararonEnRango5C(idRuta, horaInicio, horaFin);
	}
}
