package model.vo;

public class VOCalndar_Dates {
	
	private String serviceId;
	private String date;
	private String exceptionType;
	
	public  VOCalndar_Dates(String pServiceId, String pDate,String pExceptionType ){

		serviceId =pServiceId;
		date = pDate;
		exceptionType = pExceptionType;
	}
	
	public String darServiceId(){
		return serviceId;
	}
	public String darDate(){
		return date;
	}
	public String darExceptionType(){
		return exceptionType;
	}

}
