package model.vo;

public class VORuta 
{
	//Atributos

	/**
	 * Modela el id de la ruta
	 */
	private int routeId;

	/**
	 * Modela la agencia de la ruta
	 */
	private String routeAgency;

	/**
	 * Modela el nombre largo de la ruta
	 */
	private String routeLon_Name;

	/**
	 * Modela la descripci�n de la ruta
	 */
	private String routeDescript;

	/**
	 * Modela el tipo de la ruta
	 */
	private int routeType;

	/**
	 * Modela el url de la ruta
	 */
	private String routeURL;

	/**
	 * Modela el color de la ruta
	 */
	private String routeColor;

	/**
	 * Modela el color del texto de la ruta
	 */
	private String routeTxtColor;

	/**
	 * Model el short name de la ruta
	 */
	private String routeSh_Name;

	//Constructor

	public VORuta(Integer id, String agency, String sh_Name, String lo_Name, String descr, String type,String url, String col, String txt_Col)
	{
		routeId =  id;
		routeAgency = agency;
		routeColor = col;
		routeDescript = descr;
		routeLon_Name= lo_Name;
		routeSh_Name = sh_Name;
		routeTxtColor= txt_Col;
		routeType = Integer.parseInt(type);
		routeURL = url;
	}

	/**
	 * @return the routeId
	 */
	public int getRouteId() {
		return routeId;
	}

	/**
	 * @param routeId the routeId to set
	 */
	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}

	/**
	 * @return the routeAgency
	 */
	public String getRouteAgency() {
		return routeAgency;
	}

	/**
	 * @param routeAgency the routeAgency to set
	 */
	public void setRouteAgency(String routeAgency) {
		this.routeAgency = routeAgency;
	}

	/**
	 * @return the routeLon_Name
	 */
	public String getRouteLon_Name() {
		return routeLon_Name;
	}

	/**
	 * @param routeLon_Name the routeLon_Name to set
	 */
	public void setRouteLon_Name(String routeLon_Name) {
		this.routeLon_Name = routeLon_Name;
	}

	/**
	 * @return the routeDescript
	 */
	public String getRouteDescript() {
		return routeDescript;
	}

	/**
	 * @param routeDescript the routeDescript to set
	 */
	public void setRouteDescript(String routeDescript) {
		this.routeDescript = routeDescript;
	}

	/**
	 * @return the routeType
	 */
	public int getRouteType() {
		return routeType;
	}

	/**
	 * @param routeType the routeType to set
	 */
	public void setRouteType(int routeType) {
		this.routeType = routeType;
	}

	/**
	 * @return the routeURL
	 */
	public String getRouteURL() {
		return routeURL;
	}

	/**
	 * @param routeURL the routeURL to set
	 */
	public void setRouteURL(String routeURL) {
		this.routeURL = routeURL;
	}

	/**
	 * @return the routeColor
	 */
	public String getRouteColor() {
		return routeColor;
	}

	/**
	 * @param routeColor the routeColor to set
	 */
	public void setRouteColor(String routeColor) {
		this.routeColor = routeColor;
	}

	/**
	 * @return the routeTxtColor
	 */
	public String getRouteTxtColor() {
		return routeTxtColor;
	}

	/**
	 * @param routeTxtColor the routeTxtColor to set
	 */
	public void setRouteTxtColor(String routeTxtColor) {
		this.routeTxtColor = routeTxtColor;
	}

	/**
	 * @return the routeSh_Name
	 */
	public String getRouteSh_Name() {
		return routeSh_Name;
	}

	/**
	 * @param routeSh_Name the routeSh_Name to set
	 */
	public void setRouteSh_Name(String routeSh_Name) {
		this.routeSh_Name = routeSh_Name;
	}

	//M�todos


}
