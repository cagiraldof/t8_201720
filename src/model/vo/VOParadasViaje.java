package model.vo;

import model.data_structures.*;

/**
 * Clase enfocada en el retorno del requerimiento 5C que referencia un viaje y unas de sus paradas, que 
 * cumplen con le rango horario dado.
 * @author 
 *
 */
public class VOParadasViaje {
	/**
	 * id del viaje
	 */
	private Long idViaje;
	
	/**
	 * hora de inicio del rango horario analizado
	 */
	private String horaInicio;
	
	/**
	 * hora de fin del rango horario analizado
	 */
	private String horaFin;
	
	/**
	 * Lista de paradas que est�n dentro del rango horario
	 */
	private IList<VOParada> paradas;

	public Long getIdViaje() {
		return idViaje;
	}

	public void setIdViaje(Long idViaje) {
		this.idViaje = idViaje;
	}

	public String getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}

	public String getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}

	public IList<VOParada> getParadas() {
		return paradas;
	}

	public void setParadas(IList<VOParada> paradas) {
		this.paradas = paradas;
	}
	
	
	

}
