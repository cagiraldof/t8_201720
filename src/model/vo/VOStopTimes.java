package model.vo;

public class VOStopTimes {

	private int tripId;
	private int arrival;
	private int departure;
	private int stopID;
	private int stopSequence;
	private String stopHeadsign; 
	private int pickupType;
	private int  dropOffType;
	private String shapeDistTraveled;
	
	public  VOStopTimes(String pTripId, String pArrival, String pDeparture,String pStopID, String pStopSequence, String pStopHeadsign,String pPickupType, String  pDropOffType,  String pShapeDistTraveled)
	{
		tripId = Integer.parseInt(pTripId);
		arrival = Integer.parseInt(pArrival);
		departure = Integer.parseInt(pDeparture);
		stopID = Integer.parseInt(pStopID);
		stopSequence = Integer.parseInt(pStopSequence);
		stopHeadsign = pStopHeadsign;
		pickupType = Integer.parseInt(pPickupType);
		dropOffType = Integer.parseInt(pDropOffType);
		shapeDistTraveled = pShapeDistTraveled;
	}
	/**
	 * @return id - Route's id number
	 */
	public int tripId()
	{
		return tripId;
	}
	public int arrival()
	{
		return arrival;
	}
	public int departure()
	{
		return departure;
	}
	public int stopID()
	{
		return stopID;
	}
	public int stopSequence()
	{
		return stopSequence;
	}
	public String stopHeadsign()
	{
		return stopHeadsign;
	}
	public int pickupType()
	{
		return pickupType;
	}
	public int  dropOffType()
	{
		return dropOffType;
	}
	public String shapeDistTraveled()
	{
		return shapeDistTraveled;
	}
}