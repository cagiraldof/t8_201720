package model.data_structures;

public class DirectedEdge {

	private final SimpleNode  v;
	private final SimpleNode w;
	private final double weight;


	public DirectedEdge(SimpleNode v, SimpleNode w, double weight) {
		
		this.v = v;
		this.w = w;
		this.weight = weight;
	}

	public SimpleNode from() {
		return v;
	}

	
	public SimpleNode to() {
		return w;
	}

	
	public double weight() {
		return weight;
	}

	public String toString() {
		return v + "->" + w + " " + String.format("%5.2f", weight);
	}


}
