package model.data_structures;

public class SimpleNode<V, K> {
	
	// -------------------------------------------------------------
	// Atributos
	// -------------------------------------------------------------

	private SimpleNode<V, K> next;

	private V valor;
	private K id;
	
	// -------------------------------------------------------------
	// M�todos
	// -------------------------------------------------------------
	
	public SimpleNode(V pValor, K pLlave){
		next = null;
		this.valor = pValor;
		this.id = pLlave;
	}

	public SimpleNode<V, K> getNext(){
		return next;
	}

	public void setNext(SimpleNode<V, K> next){
		this.next= next;
	}

	public V getValor(){
		return valor;
	}

	public void setValor(V item){
		this.valor= item;
	}

	public K getLlave(){
		return id;
	}
	
	public void setLlave(K pLlave){
		id = pLlave;
	}

}
