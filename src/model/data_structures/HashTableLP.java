package model.data_structures;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Iterator;

import model.vo.VOViaje;

public class HashTableLP <Key,Value> implements IHashTableLP<Key,Value> {
	private int N;
	private int M=16;
	private Key[] keys;
	private Value[] vals;

	public HashTableLP()
	{
		N=0;
		keys = (Key[]) new Object[M];
		vals = (Value[]) new Object[M];

	}
	public HashTableLP(int i)
	{   N=0;
	M=i;
	keys = (Key[]) new Object[M];
	vals = (Value[]) new Object[M];


	}
	private int hash(Key key)
	{ 
		return (key.hashCode() & 0x7fffffff) % M; 
	}

	public void put(Key key, Value val)
	{

		if (N >= M/2) resize(2*M); // double M (see text)
		int i;
		for (i = hash(key); keys[i] != null; i = (i + 1) % M)
			if (keys[i].equals(key)) { vals[i] = val; return; }
		keys[i] = key;
		vals[i] = val;
		N++;
	}
	public boolean isEmpty() {
		boolean x=true;
		if (N!=0)
			x=false;
		return x;
	}
	public int darN() {
		return N;
	}
	public Value get(Key key)
	{
		for (int i = hash(key); keys[i] != null; i = (i + 1) % M)
			if (keys[i].equals(key))
				return vals[i];
		return null;
	}
	public void delete(Key key)
	{
		if (!contains(key)) return;
		int i = hash(key);
		while (!key.equals(keys[i]))
			i = (i + 1) % M;
		keys[i] = null;
		vals[i] = null;
		i = (i + 1) % M;
		while (keys[i] != null)
		{
			Key keyToRedo = keys[i];
			Value valToRedo = vals[i];
			keys[i] = null;
			vals[i] = null;
			N--;
			put(keyToRedo, valToRedo);
			i = (i + 1) % M;
		}
		N--;
		if (N > 0 && N == M/8) resize(M/2);
	}
	private boolean contains(Key key) 
	{
		boolean ret=false;
		if (this.get(key)!=null) 
		{
			ret=true;
		}
		return ret;
	}
	//	public void mostrarArreglo() {
	//		try {
	//			FileWriter fw=new FileWriter("MuestraDelHash.txt");
	//			int i =0;
	//			while (i<M) {
	//				System.out.println(keys[i]);
	////				Key st=keys[i];
	////				if(st==null) 
	////					fw.write("\n");
	////				fw.write(st.toString());
	//				
	//			}
	//			fw.close();
	//		}
	//		catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//	}

	private void resize(int cap)
	{
		HashTableLP<Key, Value> t;
		t = new HashTableLP<Key, Value>(cap);
		for (int i = 0; i < M; i++)
			if (keys[i] != null)
				t.put(keys[i], vals[i]);
		keys = t.keys;
		vals = t.vals;
		M = t.M;
	}

	public Iterator<Key> keys(){

		ArrayList<Key> llaves = new ArrayList<Key>();
		for (int i = 0; i < M; i++) {
			llaves.add(keys[i]);
		}

		return llaves.iterator();
	}

	public Iterator<Value> values(){
		ArrayList<Value> valores = new ArrayList<Value>();
		for(int i= 0; i<M;i++){
			valores.add(vals[i]);
		}
		return valores.iterator();
	}




}
