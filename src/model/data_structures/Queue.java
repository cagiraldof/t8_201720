package model.data_structures;

import java.util.Iterator;

public class Queue<E> implements IQueue<E>, Iterable<E>
{
	private Node first;
	private Node last;
	
	public Node darFirst(){
		return first;
	}
	
	public class Node {
		E item;
		Node next;
		public E darItem(){
			return item;
		}
		public Node darNext(){
			return next;
		}
	}

	public boolean isEmpty(){
		if (first==null){
			return true;
		}
		return false;
	}

	@Override
	public void enqueue(E item) {
		Node oldLast = last;
		last = new Node();
		last.item = item;
		last.next= null;
		if (isEmpty()) 
			first = last;
		else          
			oldLast.next = last;
	}

	@Override
	public E dequeue() {
		E item = first.item;
		first = first.next;
		if (isEmpty()) 
			last = null;
		return item;
	}

	public int size(){
		int size = 0;
		Iterator<E> iterator = new ListIterator();
		while(iterator.hasNext()){
			size = size +1;
			iterator.next();
		}
		return size;
	}

	@Override
	public Iterator<E> iterator() {
		return new ListIterator();
	}

	private class ListIterator implements Iterator<E>
	{
		private Node current = first;
		
		public boolean hasNext() {
			return current != null; 
		}
		
		public E next(){
			E item = current.item;
			current   = current.next; 
			return item;
		}
	}
}
