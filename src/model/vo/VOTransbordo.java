package model.vo;

import model.data_structures.IList;

public class VOTransbordo
{
	/**
	 * Modela el tiempo de transbordo
	 */
	private int transferTime;
	
	/**
	 * Id de la parada de origen.
	 */
	private Long idParadaOrigen;
	
	/**
	 * Lista de paradas que conforman el transbordo
	 */
	private IList<VOParada> listadeParadas;

	/**
	 * @return the transferTime
	 */
	public int getTransferTime()
	{
		return transferTime;
	}

	/**
	 * @param transferTime the transferTime to set
	 */
	public void setTransferTime(int transferTime) 
	{
		this.transferTime = transferTime;
	}

	/**
	 * @return the listadeParadas
	 */
	public IList<VOParada> getListadeParadas()
	{
		return listadeParadas;
	}

	public Long getIdParadaOrigen() {
		return idParadaOrigen;
	}

	public void setIdParadaOrigen(Long idParadaOrigen) {
		this.idParadaOrigen = idParadaOrigen;
	}

	/**
	 * @param listadeParadas the listadeParadas to set
	 */
	public void setListadeParadas(IList<VOParada> listadeParadas) 
	{
		this.listadeParadas = listadeParadas;
	}
	

}
