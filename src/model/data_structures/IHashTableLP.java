package model.data_structures;

import java.util.Iterator;

public interface IHashTableLP<Key,Value> {

	public void put(Key key, Value val);
	public Value get(Key key);
//	public void mostrarArreglo();
	public boolean isEmpty();
	public int darN();
	public Iterator<Key> keys();
	public Iterator<Value> values();
}
