package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

public class LinkedList<T> implements IList<T>
{
	private Node<T> primera = null;
	private Node<T> ultima = null;
	private int tamahno = 0;
	private Node<T> actualL = primera;
	
	public LinkedList()
	{
	}
	
	public void set(int pos, T pElemento)
	{
		Node<T> actual = primera;
		if(actual == null || pos > tamahno || pos < 0) throw new IndexOutOfBoundsException();
		for (int i = 0; i != pos; i++, actual = actual.siguiente);
		actual.elemento = pElemento;
	}
	
	public void remove(int pos)
	{
		Node<T> actual = primera;
		if(actual == null || pos > tamahno || pos < 0) throw new IndexOutOfBoundsException();
		else if(pos == 0) primera = actual.siguiente;
		else
		{
			for(int i = 0; i != pos--; i++ , actual = actual.siguiente);
			actual.siguiente = actual.siguiente.siguiente;
		}
	}
	
	public void add(int pos, T elemento)
	{
		Node<T> actual = primera;
		if(actual == null || pos > tamahno || pos < 0) throw new IndexOutOfBoundsException();
		else if(pos == 0) primera = new Node<T>(elemento, actual);
		else
		{
			for(int i = 0; i != pos--; i++ , actual = actual.siguiente);
			Node<T> siguienteAct = actual.siguiente;
			actual.siguiente = new Node<T>(elemento, siguienteAct);
		}
	}
	
	public int IndexOf(T elemento)
	{
		int  resp = -1;
		if(elemento == null || primera == null) return resp;
		else
		{
			boolean encontrado = false; 
			Node<T> actual = primera;
			for (resp = 0; resp < tamahno && !encontrado; resp++, actual = actual.siguiente) 
				if(actual.elemento == elemento) return resp;
		}
		return resp;
	}
	
	public boolean isEmpty()
	{
		if(primera == null)
			return true;
		return false;
	}
	
	public boolean contains(T elemento)
	{
		if(primera == null) return false;
		else
		{
			boolean encontrado = false; 
			Node<T> actual = primera;
			for (int i = 0; i < tamahno && !encontrado; i++, actual = actual.siguiente) 
				if(actual.elemento == elemento) return true;
			return false;
		}
	}
	
	public boolean contains(T[] elementos)
	{
		if(primera == null) return false;
		else
		{
			int correctos = 0;
			Node<T> actual = primera;
			for (int i = 0; i < elementos.length; i++) 
			{
				boolean encontrado = false;
				for (int j = 0; j < tamahno && !encontrado; j++, actual = actual.siguiente)
					if(actual.elemento == elementos[i]) correctos ++; encontrado = true;
			}
			if(correctos == elementos.length) return true;
			return false;
		}
	}
	
	public T[] toArray()
	{
		ArrayList<T> resp = new ArrayList<T>();
		Node<T> actual = primera;
		for (; actual != null; actual = actual.siguiente) 
		{
			resp.add(actual.elemento);
		}
		T[] array = (T[]) resp.toArray();
		return array;
 	}
	
	public void clear()
	{
		ultima = primera = null;
	}
	
	public boolean add(T object) {
		// TODO Auto-generated method stub
		boolean r=false;
		if(primera == null){
			ultima = primera = new Node<T>(object, null); tamahno++;
			r=true;
		}
		else{
			primera = new Node<T>(object, primera); tamahno++;
			r=true;
		}
		return r;
	}
	
	public boolean addAtEnd(T object) {
		// TODO Auto-generated method stub
		boolean r=false;
		if(primera == null){
			ultima = primera = new Node<T>(object, null); tamahno++;
			r=true;
		}
		else{
			ultima.siguiente = new Node<T>(object, null); ultima = ultima.siguiente; tamahno++;
			r=true;
		}
		return r;
	}

	public boolean addAtK(T object, int K) {
		// TODO Auto-generated method stub
		Node<T> actual = primera;
		boolean r=false;
		if(K > tamahno || K < 0) throw new IndexOutOfBoundsException();
		if(K == 0){
			add(object);
			r=true;
		}
		else if(K == tamahno-1){
			addAtEnd(object);
			r=true;
		}
		else
		{
			for(int i = 0; i != K--; i++ , actual = actual.siguiente);
			Node<T> x = new Node<T>(object, actual.siguiente);
			actual.siguiente = x;
			tamahno++;
			r=true;
		}
		return r;
	}

	public T getElement(int k) {
		// TODO Auto-generated method stub
		Node<T> actual = primera;
		if(actual == null || k > tamahno || k < 0) throw new IndexOutOfBoundsException();
		for (int i = 0; i != k; i++, actual = actual.siguiente);
		return actual.elemento;
	}

	public T getCurrentElement() {
		// TODO Auto-generated method stub
		return actualL.elemento;
	}

	public void next() {
		// TODO Auto-generated method stub
		if(actualL.siguiente != null) actualL = actualL.siguiente;
	}

	public void previous() {
		// TODO Auto-generated method stub
		Node<T> actual = primera;
		for (int i = 0; i < tamahno && actual.siguiente == actualL; i++, actual = actual.siguiente);
		actualL = actual;
	}

	public long getSize() {
		// TODO Auto-generated method stub
		return tamahno;
	}

	public T delete(T object) {
		// TODO Auto-generated method stub
		T eliminado = null;
		boolean eliminar = false;
		Node<T> actual = primera;
		if(actual == null) throw new IndexOutOfBoundsException();
		if(actual.elemento == object){
			eliminado = actual.elemento; primera = actual.siguiente;
			if(eliminado == actualL.elemento){
				if(actualL.siguiente != null)next();
				else actualL = null;
			}
		}
		else{
			for(int i = 0; i < tamahno && !eliminar; i++, actual = actual.siguiente){
			if(actual.siguiente.elemento == object){
				eliminado = actual.siguiente.elemento; 
				actual.siguiente = actual.siguiente.siguiente; eliminar = true; tamahno--;
				if(actualL.elemento == eliminado){
					if(actualL.siguiente != null) next();
					else previous();
					}
				}
			}
		}
		return eliminado;
	}

	public T deleteAtK(int k) {
		// TODO Auto-generated method stub
		T eliminado = null;
		Node<T> actual = primera;
		if(actual == null || k > tamahno || k < 0) throw new IndexOutOfBoundsException();
		if(k == 0) {
			eliminado = primera.elemento; primera = actual.siguiente; 
			tamahno--;
			if(eliminado == actualL.elemento){
				if(actualL.siguiente != null)next();
				else actualL = null;
			}
		}
		else if(k == tamahno-1) {
			eliminado = ultima.elemento; tamahno--;
			Node<T> ultimaN = primera;
			for (int i = 0; i < tamahno && ultimaN.siguiente == ultima; i++, ultimaN = ultimaN.siguiente);
			ultima = ultimaN;
			if(actualL.elemento == eliminado) previous();
		}
		else
		{
			for(int i = 0; i != k--; i++ , actual = actual.siguiente);
			eliminado = actual.siguiente.elemento;
			actual.siguiente = actual.siguiente.siguiente; tamahno--;
			if(actualL.elemento == eliminado){
				if(actualL.siguiente != null) next();
				else previous();
			}
		}
		return eliminado;
	}

	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new MiIterator<T>(primera);
	}
	
	private static class Node<T>
	{
		T elemento;
		Node<T> siguiente;
		
		public Node(T pElemento, Node<T> pSiguiente)
		{
			elemento = pElemento;
			siguiente = pSiguiente;
		}
	}
	
	public class MiIterator<T> implements Iterator<T>
	{
		Node<T> actual;
		public MiIterator(Node<T> primera){
			actual = primera;
		}

		public boolean hasNext() {
			// TODO Auto-generated method stub
			return actual != null;
		}

		public T next() {
			// TODO Auto-generated method stub
			T elemento = actual.elemento;
			actual = actual.siguiente;
			return elemento;
		}
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return tamahno;
	}
}
