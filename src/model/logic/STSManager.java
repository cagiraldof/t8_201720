package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;

import API.ISTSManager;
import model.data_structures.HashTableLP;
import model.data_structures.IHashTableLP;
import model.data_structures.IList;
import model.data_structures.IMinBinHeap;
import model.data_structures.IQueue;
import model.data_structures.IRedBlackBST;
import model.data_structures.LinkedList;
import model.data_structures.MinBinHeap;
import model.data_structures.Queue;
import model.vo.VOAgency;
import model.vo.VOBusUpdate;
import model.vo.VOCalndar_Dates;
import model.vo.VOParada;
import model.vo.VOParadasViaje;
import model.vo.VORangoHora;
import model.vo.VORetardo;
import model.vo.VORuta;
import model.vo.VOStopTimes;
import model.vo.VOTransbordo;
import model.vo.VOViaje;

public class STSManager implements ISTSManager
{
	IQueue <VOBusUpdate> queueBusUpdates;
	IHashTableLP<Integer,VORuta> hashRutas;
	IMinBinHeap<VOParada> heapParadas;
	LinkedList <VOAgency> listaAgency;
	IHashTableLP<Integer,VOViaje> hashViajes;
	
	LinkedList<VOStopTimes> listaStopT;
	IHashTableLP<Integer,VOCalndar_Dates> hashFechas;





	@Override
	public void ITSInit() 
	{
		queueBusUpdates= new Queue<VOBusUpdate>();
		hashRutas=new HashTableLP<Integer, VORuta>();
		heapParadas=new MinBinHeap<VOParada>();
		listaAgency=new LinkedList<VOAgency>();
		hashViajes= new HashTableLP<Integer, VOViaje>() ;
		System.out.println("ITSInit");

	}

	@Override
	public void ITScargarGTFS1C() 
	{
		System.out.println("ITScargarGTFS1C");
		loadRutas();
		loadParadas();
		loadAgency();
		loadViajes();
		
	}

	@Override
	public void ITScargarTR(String fecha) 
	{
		loadBusUpdate(fecha);
		System.out.println("ITScargarTR " + fecha);
	}



	@Override
	public IList<VOViaje> ITSviajesHuboRetardoParadas1A(String idRuta, String fecha) {
		// TODO Auto-generated method stub

		this.ITScargarTR(fecha);

		Queue<VOBusUpdate> busesEnRuta = new Queue<VOBusUpdate>();
		Iterator<VOBusUpdate> t = queueBusUpdates.iterator();

		while(t.hasNext()) {
			VOBusUpdate buz=t.next();
			int routeNo= buz.getRouteNo();
			int routeID=this.routeIDFromShortName(routeNo);
			if(routeID!=-1&&routeID==Integer.parseInt(idRuta)) {
				busesEnRuta.enqueue(buz);
			}
		}
	

		System.out.println("ITSviajesHuboRetardoParadas1A " + idRuta + " - " + fecha);
		return null;
	}
	
	public int routeIDFromShortName(int routeShNm) {
		Iterator<VORuta> e = hashRutas.values();
		Boolean encontrado =false;
		int idRutaEncontrada=-1;
		while(e.hasNext()&&!encontrado) {
			VORuta esta=e.next();
			if (Integer.parseInt(esta.getRouteSh_Name())==routeShNm){
				idRutaEncontrada=esta.getRouteId();
				encontrado=true;
			}
		}
		return idRutaEncontrada;
	}

	@Override
	public IList<VOParada> ITSNParadasMasRetardos2A(String fecha, int n) {
		// TODO Auto-generated method stub
		System.out.println("ITSNParadasMasRetardos2A " + fecha + " - " + n);
		return null;
	}

	@Override
	public IList<VOTransbordo> ITStransbordosRuta3A(String idRuta, String fecha) {
		// TODO Auto-generated method stub
		System.out.println("ITStransbordosRuta3A " + idRuta + " - " + fecha);
		return null;
	}

	@Override
	public IList<VOViaje> ITSviajesRetrasoTotalRuta1B(String idRuta, String fecha)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VORangoHora ITSretardoHoraRuta2B(String idRuta, String fecha)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOViaje> ITSbuscarViajesParadas3B(String idOrigen, String idDestino, String fecha, String horaInicio,
			String horaFin) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOViaje> ITSNViajesMasDistancia2C(int n, String fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IRedBlackBST<Integer, IList<VORetardo>> retardosViajeFecha3C(String idViaje, String fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOParada> ITSParadasCompartidas4C(String fecha)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOParadasViaje> ITSViajesPararonEnRango5C(String idRuta, String horaInicio, String horaFin) {
		// TODO Auto-generated method stub
		return null;
	}

	public void loadRutas() {
		try {
			File pArchivoRuta = new File("./data/routes.txt");

			BufferedReader bf2 = new BufferedReader( new FileReader( pArchivoRuta ) );
			String linea2 = bf2.readLine( );
			linea2 = bf2.readLine( );


			while( linea2 != null )
			{
				String[] partesDato = linea2.split( "," );
				VORuta ruta = new VORuta(Integer.parseInt(partesDato[0]),partesDato[1], partesDato[2], partesDato[3], partesDato[4],partesDato[5]
						,partesDato[6],partesDato[7],partesDato[8]);
				hashRutas.put(ruta.getRouteId(),ruta);
				linea2=bf2.readLine();
			}
			bf2.close( );
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void loadParadas() {
		try {
			File pArchivoStop = new File("./data/stops.txt");

			BufferedReader bf2 = new BufferedReader( new FileReader( pArchivoStop ) );
			String linea2 = bf2.readLine( );
			linea2 = bf2.readLine( );
			while( linea2 != null )
			{

				String[] partesDato = linea2.split( "," );

				//Hay un error en el formato, el �ltimo elemento no se agrega a la lista por ser una coma antes del \n (el split no detecta ningun string ah�)
				if (partesDato.length==9) {
					partesDato=new String[10];
					partesDato[0]=linea2.split( "," )[0];
					partesDato[1]=linea2.split( "," )[1];
					partesDato[2]=linea2.split( "," )[2];
					partesDato[3]=linea2.split( "," )[3];
					partesDato[4]=linea2.split( "," )[4];
					partesDato[5]=linea2.split( "," )[5];
					partesDato[6]=linea2.split( "," )[6];
					partesDato[7]=linea2.split( "," )[7];
					partesDato[8]=linea2.split( "," )[8];
					partesDato[9]="";
				}
				String stopID=partesDato[0].trim();
				String stopCode=partesDato[1].trim();
				String stopName=partesDato[2].trim();
				String stopDesc=partesDato[3].trim();
				String stopLat=partesDato[4].trim();
				String stopLon=partesDato[5].trim();
				String stopZoneID=partesDato[6].trim();
				String stopURL=partesDato[7].trim();
				String stopLocType=partesDato[8].trim();
				String stopParentSt=partesDato[9].trim();

				VOParada stop = new VOParada(stopID, stopCode, stopName, stopDesc, stopLat, stopLon, stopZoneID, stopURL, stopLocType, stopParentSt);

				heapParadas.insert(stop);
				linea2=bf2.readLine();
			}


			bf2.close( );
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	public void loadBusUpdate(String fecha) {

		File encontrado=buscarUpdatePorFecha(fecha);
		BufferedReader  reader=null;
		try {

			reader= new BufferedReader(new FileReader(encontrado));

			JsonReader reader2= new JsonReader(reader);
			Gson gson= new GsonBuilder().create();			
			reader2.beginArray();



			while(reader2.hasNext()){
				VOBusUpdate bus= gson.fromJson(reader2, VOBusUpdate.class);
				queueBusUpdates.enqueue(bus);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}

	}

	private File buscarUpdatePorFecha(String fecha) {
		// No entiendo c�mo est�n tageadas las fechas en los archivos json bus update
		return new File("./data/RealTime-8-21-BUSES_SERVICE/BUSES_SERVICE_0.json");
	}
	
	private void loadAgency() {
		try {
			File pArchivoRuta = new File("./data/agency.txt");

			BufferedReader bf2 = new BufferedReader( new FileReader( pArchivoRuta ) );
			String linea2 = bf2.readLine( );
			linea2 = bf2.readLine( );


			while( linea2 != null )
			{
				String[] partesDato = linea2.split( "," );
				VOAgency agencia = new VOAgency(partesDato[0],partesDato[1], partesDato[2], partesDato[3], partesDato[4]);
				listaAgency.add(agencia);
				linea2=bf2.readLine();
			}
			bf2.close( );
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void loadViajes() {
		try {
			File pArchivoRuta = new File("./data/trips.txt");

			BufferedReader bf2 = new BufferedReader( new FileReader( pArchivoRuta ) );
			String linea2 = bf2.readLine( );
			linea2 = bf2.readLine( );


			while( linea2 != null )
			{
				String[] partesDato = linea2.split( "," );
				VOViaje trip = new VOViaje(Integer.parseInt(partesDato[0]),Integer.parseInt(partesDato[1]),Integer.parseInt(partesDato[2]),partesDato[3],partesDato[4],Integer.parseInt(partesDato[5]),Integer.parseInt(partesDato[6]),Integer.parseInt(partesDato[7]),Boolean.parseBoolean(partesDato[8]),Boolean.parseBoolean(partesDato[9]));
				hashViajes.put(trip.getTripID(),trip);
				linea2=bf2.readLine();
			}
			bf2.close( );
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
